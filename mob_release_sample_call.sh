#!/bin/bash

## O ID numérico da aplicação no Mobrelease
export MR_APPLICATION_ID=1

## Idem para a plataforma
export MR_PLATFORM_ID=7

## A URL do Mobrelease com o path de execução de comandos na API
export MR_API_URL=http://localhost/mobrelease/api/v1

## O Usuário que vai uplodear
export MR_USER=deus

## A KEY desse usuário
export MR_KEY=d4c51aa3c9654495ffc2d5c344bb9fe5adfe9260

## A Versão que voce quer subir
export VERSION=""

## Uma descrição para essa versão
export RELEASE_NOTE=""

## Esse read maroto cria uma variável com um JSON montadinho no 
## formato que o Mobrelease entende
read -r -d '' MEU_JSON <<ENDVAR
{
	"application":    "/api/v1/application/$MR_APPLICATION_ID/",
	"platform":       "/api/v1/platform/$MR_PLATFORM_ID/",
	"version":        "$VERSION",
	"release_notes":  "$RELEASE_NOTE"
}
ENDVAR

## Cria um release no servidor Mobrelease. Este release receberá
## posteriormente um arquivo
MR_API_RELEASE="${MR_API_URL}/release/?format=json&username=${MR_USER}&api_key=${MR_KEY}"
RESP_JSON="$(curl -v -s -H "Content-Type: application/json" -X POST --data "$MEU_JSON" "${MR_API_RELEASE}")"

## Cria uma URL para Upload com o Release ID correto, extraído do 
## JSON de resposta.
RELEASE_ID="$(echo "$RESP_JSON" | egrep -o '\"id\":\ "[0-9a-zA-Z\-\.]*\"' | cut -d ':' -f 2 | tr -d '" ')"
MR_API_RELEASE_FILE="${MR_API_URL}/releasefile/$RELEASE_ID/?format=json&username=${MR_USER}&api_key=${MR_KEY}"

## Manda um arquivo para o Release criado anteriormente
curl -v -X POST -F file="@$1" "$MR_API_RELEASE_FILE"
