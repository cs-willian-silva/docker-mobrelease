# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.template import RequestContext
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404, get_list_or_404, redirect
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from models import *
from setuptools.command.sdist import re_finder


def get_applications(request):
    
    applications = get_list_or_404(Application)
    
    context = { 'title': 'Applications', 'applications': applications }
    
    return HttpResponse(render_to_string("mobrelease/applications_list.html", context_instance=RequestContext(request, context)))

def get_platforms(request, app_slug):
    
    application = get_object_or_404(Application, slug=app_slug)
    platforms = application.platforms()
    
    title = application.name
    
    context = { 'title': title, 'application': application, 'platforms': platforms }
    
    return HttpResponse(render_to_string("mobrelease/platforms_list.html", context_instance=RequestContext(request, context)))

def get_last_releases(request, app_slug):
    
    application = get_object_or_404(Application, slug=app_slug)
    platforms = application.platforms()
    releases = [application.last_release(platform) for platform in platforms ]
    
    title = application.name
    
    context = { 'title': title, 'application': application, 'releases': releases }
        
    return HttpResponse(render_to_string("mobrelease/last_releases_list.html", context_instance=RequestContext(request, context)))

def get_releases(request, app_slug, platform_slug):
    
    application = get_object_or_404(Application, slug=app_slug)
    platform = get_object_or_404(Platform, slug=platform_slug)
    releases = get_list_or_404(Release, (Q(application=application) & Q(platform=platform)))
    
    title = '%s - %s' % (application.name, platform.name)

    context = { 'title': title, 'application': application, 'platform' : platform, 'releases': releases }
        
    return HttpResponse(render_to_string("mobrelease/releases_list.html", context_instance=RequestContext(request, context)))

def gerenate_release(request, app_slug, platform_slug, version):

    application = get_object_or_404(Application, slug=app_slug)
    platform = get_object_or_404(Platform, slug=platform_slug)
    release = get_object_or_404(Release, (Q(application=application) & Q(platform=platform) & Q(version=version)))

    title = '%s - %s v%s' % (application.name, platform.name, release.version)

    release_download_url = release.file.url
    if release.is_IOS():
        # itms-services://?action=download-manifest&url=https://mobiledeviphone.concretecorp.com.br/mobrelease/comprafaciliphone/189/app.plist
        release_download_url = "itms-services://?action=download-manifest&url=https://%s/mobrelease/application/%s/%s/%s/app.plist" % ( request.META['HTTP_HOST'], application.slug, platform.slug, release.version )
    else:
        release_download_url = request.build_absolute_uri(release_download_url)

    qrcode_url = request.build_absolute_uri(request.get_full_path()) + 'qrcode/'

    context = { 'title': title, 'application': application, 'platform' : platform, 'release': release, 'release_download_url' : release_download_url, 'qrcode_url' : qrcode_url }

    return context

def get_release(request, app_slug, platform_slug, version):
    
    context = gerenate_release(request, app_slug, platform_slug, version)

    return HttpResponse(render_to_string("mobrelease/release.html", context_instance=RequestContext(request, context)))

def get_release_to_qrcode(request, app_slug, platform_slug, version):

    context = gerenate_release(request, app_slug, platform_slug, version)

    return HttpResponse(render_to_string("mobrelease/release_download_qrcode.html", context_instance=RequestContext(request, context)))

def get_release_ios(request, app_slug, platform_slug, version):
    
    application = get_object_or_404(Application, slug=app_slug)
    platform = get_object_or_404(Platform, slug=platform_slug)
    release = get_object_or_404(Release, (Q(application=application) & Q(platform=platform) & Q(version=version)))
    
    title = '%s - %s v%s' % (application.name, platform.name, release.version)
    
    context = { 'title': title, 'application': application, 'platform' : platform, 'release': release, 'host' : request.META['HTTP_HOST'] }
        
    return HttpResponse(render_to_string("mobrelease/ios_ota.plist", context_instance=RequestContext(request, context)))

@login_required
def reset_api_key(request, user_id):
    
    from django.contrib.auth.admin import User
    
    user = get_object_or_404(User, id=user_id)
 
    from tastypie.models import ApiKey
    try:
        api_key = ApiKey.objects.get(user=user)
        api_key.key = None
        api_key.save()
    
    except ApiKey.DoesNotExist:
        api_key = ApiKey.objects.create(user=user)
    
    return redirect(reverse('admin:auth_user_changelist'))
