# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from apps.mobrelease.views import *

urlpatterns = patterns('',
    url(r'^reset_api_key/(?P<user_id>[\d]+)/$', reset_api_key, name='reset_api_key'),
    (r'^applications/$', get_applications),
    (r'^application/(?P<app_slug>[\w-]+)/$', get_platforms),
    (r'^application/(?P<app_slug>[\w-]+)/last_releases/$', get_last_releases),
    (r'^application/(?P<app_slug>[\w-]+)/(?P<platform_slug>[\w-]+)/$', get_releases),
    (r'^application/(?P<app_slug>[\w-]+)/(?P<platform_slug>[\w-]+)/(?P<version>[\w\.-]+)/$', get_release),
    (r'^application/(?P<app_slug>[\w-]+)/(?P<platform_slug>[\w-]+)/(?P<version>[\w\.-]+)/qrcode/$', get_release_to_qrcode),
    (r'^application/(?P<app_slug>[\w-]+)/(?P<platform_slug>[\w-]+)/(?P<version>[\w\.-]+)/app.plist$', get_release_ios)
)