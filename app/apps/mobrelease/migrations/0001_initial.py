# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Application'
        db.create_table('mobrelease_application', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=120)),
            ('logo', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('bundle_identifier', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=1000)),
        ))
        db.send_create_signal('mobrelease', ['Application'])

        # Adding model 'Platform'
        db.create_table('mobrelease_platform', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('slug', self.gf('django.db.models.fields.SlugField')(unique=True, max_length=50, blank=True)),
        ))
        db.send_create_signal('mobrelease', ['Platform'])

        # Adding model 'Release'
        db.create_table('mobrelease_release', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('application', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mobrelease.Application'])),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['mobrelease.Platform'])),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('release_date', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('release_notes', self.gf('django.db.models.fields.TextField')(max_length=3000)),
        ))
        db.send_create_signal('mobrelease', ['Release'])

        # Adding unique constraint on 'Release', fields ['application', 'platform', 'version']
        db.create_unique('mobrelease_release', ['application_id', 'platform_id', 'version'])


    def backwards(self, orm):
        # Removing unique constraint on 'Release', fields ['application', 'platform', 'version']
        db.delete_unique('mobrelease_release', ['application_id', 'platform_id', 'version'])

        # Deleting model 'Application'
        db.delete_table('mobrelease_application')

        # Deleting model 'Platform'
        db.delete_table('mobrelease_platform')

        # Deleting model 'Release'
        db.delete_table('mobrelease_release')


    models = {
        'mobrelease.application': {
            'Meta': {'ordering': "['name']", 'object_name': 'Application'},
            'bundle_identifier': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '120'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'})
        },
        'mobrelease.platform': {
            'Meta': {'ordering': "['name']", 'object_name': 'Platform'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'slug': ('django.db.models.fields.SlugField', [], {'unique': 'True', 'max_length': '50', 'blank': 'True'})
        },
        'mobrelease.release': {
            'Meta': {'ordering': "['-release_date']", 'unique_together': "(('application', 'platform', 'version'),)", 'object_name': 'Release'},
            'application': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mobrelease.Application']"}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['mobrelease.Platform']"}),
            'release_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'release_notes': ('django.db.models.fields.TextField', [], {'max_length': '3000'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['mobrelease']