# -*- coding: utf-8 -*-
from django.db import models
from django.core.urlresolvers import reverse

class Application(models.Model):
    name = models.CharField('Nome', max_length=120, unique=True)
    logo = models.ImageField('Logo', upload_to='logos', null=True, blank=True)
    slug = models.SlugField('Slug', blank=True, unique=True)
    email = models.EmailField('Email', null=True, blank=True)
    bundle_identifier = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(u'Descrição', max_length=1000)
    
    def list_platforms(self):
        links = []
        for platform in self.platforms():
            name = platform.name            
            if self.last_release(platform) == []:
                continue
            pk = self.last_release(platform).pk
            link = '<a href="%s">%s</a>' % (reverse('admin:mobrelease_release_change', args=[pk]), name)
            links.append(link)
        return ', '.join(links)
    list_platforms.allow_tags = True
    list_platforms.short_description = 'Plataformas'
    
    def platforms(self):
        from django.db.models import Count
        pks = Platform.objects.filter(release__application=self.pk).annotate(count=Count('name')).values('pk')
        return Platform.objects.filter(pk__in=pks)
    
    def last_release(self, platform):
        objs = Release.objects.filter(application=self.pk).filter(platform=platform).order_by('-pk')
        if len(objs) > 0:
            return objs[0]
        else:
            return None
    
    def get_absolute_url(self):
        return reverse('apps.mobrelease.views.get_platforms', kwargs={'app_slug': self.slug})
        
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        verbose_name = u'Aplicação'
        verbose_name_plural = u'Aplicações'
    
class Platform(models.Model):
    name = models.CharField('Nome', max_length=50, unique=True)
    slug = models.SlugField('Slug', blank=True, unique=True)
    
    def __unicode__(self):
        return self.name
    
    class Meta:
        ordering = ['name']
        verbose_name = 'Plataforma'
        verbose_name_plural = 'Plataformas'

class Release(models.Model):
    application = models.ForeignKey(Application, verbose_name=u'Aplicação')
    platform = models.ForeignKey(Platform, verbose_name='Plataforma')
    version = models.CharField(u'Versão', max_length=20)
    file = models.FileField('Arquivo', upload_to='releases', null=True, blank=True)
    release_date = models.DateTimeField(u'Data de Lançamento', auto_now=True)
    release_notes = models.TextField(u'Notas de lançamento', max_length=3000)
    
    def __unicode__(self):
        return '%s v%s' % (self.application.name, self.version)

    def is_IOS(self):
        if self.platform.name == "iPhone" or self.platform.name == "iPad" or self.platform.name == "iOS":
            return True
        else:
            return False

    def get_url_download(self):
        pass


    class Meta:
        ordering = ['-release_date']
        unique_together = ('application', 'platform', 'version')


# SIGNALS
from django.db.models import signals
from django.template.defaultfilters import slugify

def slug_pre_save(signal, instance, sender, **kwargs):
    if not instance.slug:
        slug = slugify(instance.name)
        new_slug = slug
        counter = 0
        
        while sender.objects.filter(slug=new_slug).exclude(id=instance.id).count() > 0:
            counter += 1
            new_slug = '%s-%d'%(slug, counter)
            
        instance.slug = new_slug

signals.pre_save.connect(slug_pre_save, sender=Application)
signals.pre_save.connect(slug_pre_save, sender=Platform)
