# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.auth.admin import User, UserAdmin
from models import *

class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['name', 'list_platforms' , 'slug']
    
class ReleaseAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'application', 'platform']
    list_filter = ['platform']

class CustomUserAdmin(UserAdmin):
    list_display = UserAdmin.list_display + ('get_apikey',)
    
    def get_apikey(self, obj=None):
        from tastypie.models import ApiKey
        try:
            api_key = ApiKey.objects.get(user=obj)
        
        except ApiKey.DoesNotExist:
            api_key = ApiKey.objects.create(user=obj)
        
        from django.core.urlresolvers import reverse
        return '%s - <a href="%s">Reset</a>' % (api_key.key, reverse('reset_api_key', args=[obj.id]))
    get_apikey.allow_tags = True
    get_apikey.short_description = 'API Key'
    
    
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(Application, ApplicationAdmin)
admin.site.register(Platform)
admin.site.register(Release, ReleaseAdmin)
