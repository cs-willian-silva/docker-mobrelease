# -*- coding: utf-8 -*-
from tastypie.api import Api
from apps.api.v1.resources import *

v1_api = Api(api_name='v1')
v1_api.register(ApplicationResource())
v1_api.register(ApplicationLogoResource())
v1_api.register(PlatformResource())
v1_api.register(ReleaseResource())
v1_api.register(ReleaseFileResource())