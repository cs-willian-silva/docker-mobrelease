# -*- coding: utf-8 -*-
from tastypie import fields
from tastypie.http import HttpBadRequest
from tastypie.resources import ModelResource
from tastypie.authorization import DjangoAuthorization
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication
from django.http import HttpResponseNotAllowed
from django.shortcuts import get_object_or_404
from django.template.defaultfilters import slugify
from apps.mobrelease.models import Release, Platform, Application

class BaseResource(ModelResource):
    class Meta:
        always_return_data = True
        authentication = ApiKeyAuthentication()
        authorization = DjangoAuthorization()
        #serializer = Serializer(formats=['xml', 'json'])
        #excludes = ['id']
        
class ApplicationResource(BaseResource):
    
    def dehydrate_slug(self, bundle):
        if not bundle.data['slug']:
            return slugify(bundle.data['name'])
        return bundle.data['slug']
    
    def dehydrate(self, bundle):
        platforms = bundle.obj.platforms()
        if platforms:
            out = []
            for platform in platforms:
                release = bundle.obj.last_release(platform)
                last_release = {'version': release.version, 'download': release.get_url_download()}
                out.append({'name':platform.name, 'last_release':last_release})
            bundle.data['platforms'] = out
        return bundle
    
    class Meta(BaseResource.Meta):
        queryset = Application.objects.all()
    
class ApplicationLogoResource(BaseResource):
    logo = fields.FileField(attribute="logo")
    
    def dispatch_detail(self, request, pk, **kwargs):
        if request.method == 'POST':
            obj = get_object_or_404(Application, pk=pk)
            logo = request.FILES.get('logo', None)
            if logo is not None:
                obj.logo.save(logo._name, logo)
                bundle = self.build_bundle(obj)
                bundle = self.full_dehydrate(bundle)
                bundle = self.alter_detail_data_to_serialize(request, bundle)
                return self.create_response(request, bundle)
            else:
                return self.create_response(request, bundle, response_class = HttpBadRequest)

        super(ApplicationLogoResource, self).dispatch_detail(request, **kwargs)
        
    class Meta(BaseResource.Meta):
        queryset = Application.objects.all()
        allowed_methods = ['post']
        
class PlatformResource(BaseResource):
    
    class Meta(BaseResource.Meta):
        queryset = Platform.objects.all()
    
class ReleaseResource(BaseResource):
    application = fields.ForeignKey(ApplicationResource, 'application')
    platform = fields.ForeignKey(PlatformResource, 'platform')
    
    def dehydrate(self, bundle):
        bundle.data['application'] = { 'name':bundle.obj.application.name, 'resource_uri':bundle.data['application'] }
        bundle.data['platform'] = { 'name':bundle.obj.platform.name, 'resource_uri':bundle.data['platform'] }
        return bundle
        
    class Meta(BaseResource.Meta):
        queryset = Release.objects.all()
        
class ReleaseFileResource(BaseResource):
    file = fields.FileField(attribute="file")
    
    def dispatch_detail(self, request, pk, **kwargs):

        if request.method == 'POST':
            obj = get_object_or_404(Release, pk=pk)
            file = request.FILES.get('file', None)

            if file is not None:
                obj.file.save(file._name, file)
                bundle = self.build_bundle(obj)
                bundle = self.full_dehydrate(bundle)
                bundle = self.alter_detail_data_to_serialize(request, bundle)

                return self.create_response(request, bundle)
            else:
                return self.create_response(request, bundle, response_class = HttpBadRequest)

        super(ReleaseFileResource, self).dispatch_detail(request, **kwargs)
        
    class Meta(BaseResource.Meta):
        queryset = Release.objects.all()
        allowed_methods = ['post']
        