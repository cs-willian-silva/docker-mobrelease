#!/bin/bash

cd /docker-mobrelease/app 

echo CREATE DATABASE IF NOT EXISTS cs_mobrelease | mysql -uroot -pConcrete123 -h mysql

export PYTHON27=$(which python2.7)

if ! [ -e /data/not-first-run ]
then
	python manage.py syncdb --noinput
	python manage.py migrate
	python manage.py loaddata ./csmobrelease/fixtures/user.json
	python manage.py loaddata ./apps/mobrelease/fixtures/platform.json
	python manage.py collectstatic --noinput
	touch /data/not-first-run
fi

/usr/sbin/apache2ctl -D FOREGROUND
