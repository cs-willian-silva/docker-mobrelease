#!/bin/bash

export PYTHON27=$(which python2.7)
pip install -r ./requirements.pip
cd app/csmobrelease/
[ -e settings.py ] && rm -f settings.py
ln -s conf/settings_prod.py settings.py

